import Statistics
include("../funcao/permutacao.jl")
function getPermutacaoData()
	io = open("../tabela/permutacaoComparado.csv", "w")
	for j in 1:50
		v=criapermutacao(j*100)
		test_a = Statistics.median([@elapsed permuta(v) for i = 1:50])
		test_b = Statistics.median([@elapsed permuta2(v) for i = 1:50])
		test_c = Statistics.median([@elapsed permuta3(v) for i = 1:50])
		println(j*100," ",test_a," ",test_b," ",test_c)
		println(io,j*100,";",test_a,";",test_b,";",test_c)
	end
	close(io)
end
function criapermutacao(n)
	v=[]
	for i in 1:n
		push!(v,i)
	end
	u=[]
	for i in 1:n
		sorteio =rand(1:n-i+1)
		push!(u,v[sorteio])
		v[sorteio]=v[n-i+1]
		v[n-i+1]=0
	end
	return u
end