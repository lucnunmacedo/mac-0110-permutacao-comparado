#Codigo fornecido pelo Professor Alfredo Goldman Vel Lejbman
function testaPermutacao()
    if !perm([1, 2, 3])
        println("Não funcionou para [1, 2, 3]")
    end
    if perm([1, 2, 3, 3]) 
        println("Não funcionou para [1, 2, 3, 3]")
    end
    if !perm([1])
        println("Não funcionou para [1]")
    end
    if !perm([])
        println("Não funcionou para []")
    end
    println("Final dos testes")
end
function perm1(v)
   return permuta(v)
end
function perm2(v)
   return permuta2(v)
end
function perm3(v)
   return permuta3(v)
end

# versão 1
function permuta(v)
   tam = length(v)
   for i in 1:tam
      if  !esta(i, v)
        return false
      end
   end
   return true
end

function esta(i, v)
   for el in v
      if i == el
         return true
      end
   end
   return false
end        

# versão 2
# Nessa versão usamos um comando de Julia que verifica se um elemento
# pertence a um vetor (in)
function permuta2(v)
   tam = length(v)
   for i in 1:tam
      if  !(i in v)
         return false
      end
   end
   return true
end

# Versão 3
# Vamos verificar se o vetor é uma permutação com uma passagem
function permuta3(v)
   tam = length(v)
   u = zeros(Int, tam)
   for i in 1:tam
      if v[i] < 1 || v[i] > tam || u[v[i]] == 1
         return false
      end
      u[v[i]] = 1
   end
   return true
end

